#include "reader/reader.hh"
#include "reader/labelReader.hh"
#include "pca/pca.hh"
#include "em/EM.hh"
#include <cstdlib>
#include <vector>
#include "classify/classify.hh"
#include "disp.hh"
#include "singleton/singletoncust.hh"
#include <tbb/tbb.h>
#include <tbb/parallel_for.h>
#include <stdlib.h>


Eigen::MatrixXd& getDigitMatrix(Eigen::MatrixXd& data, std::vector<int> index) {
  Eigen::MatrixXd* ans = new Eigen::MatrixXd(data.rows(), index.size());

  for (int i = 0; i < index.size(); ++i) {
    for (int j = 0; j < data.rows(); ++j) {
      (*ans)(j, i) = data(j, index[i]);
    }
  }

  std::cout << "EXTRACTION DONE" << std::endl;

  return *ans;
}

Pca& learnDataset(Reader& dataReader, LabelReader& labelReader,
		  std::vector<Eigen::VectorXd>& weights,
		  std::vector<Eigen::MatrixXd>& means,
		  std::vector<std::vector<Eigen::MatrixXd>>& variances) {


  SingletonCust *singleton1;

  // initialisation des pointeurs
  singleton1 = SingletonCust::getInstance();

  Eigen::MatrixXd data = dataReader.getSampleMatrix();
  std::cout << "DATA READ " << data.rows() << " " << data.cols() << std::endl;

  Pca* pca = new Pca();
  Eigen::MatrixXd reducedData = pca->computePCA(data.adjoint(), singleton1->getPCAComponents());
  std::cout << "DATA REDUCED " << reducedData.rows() << " " << reducedData.cols() << std::endl;

  reducedData.adjointInPlace();

  tbb::tick_count startTime = tbb::tick_count::now();

  if (singleton1->getParNumber())
  {
    parallel_for(tbb::blocked_range<size_t>(0, 10, 1), [&](const tbb::blocked_range<size_t>& r){
        for (int i = r.begin(); i != r.end(); ++i) {
          Eigen::MatrixXd digitData = getDigitMatrix(reducedData, labelReader.getIndexList(i));

          srand (123145);
          EM em(singleton1->getEMMixtures(), digitData, i);

          long digitTime = em.learn(weights, means, variances);

          std::cout << "Learned digit " << i << " in " << digitTime << "ms" << std::endl;
        }
    });
  }
  else
  {
    for (int i = 0; i < 10; ++i) {
      Eigen::MatrixXd digitData = getDigitMatrix(reducedData, labelReader.getIndexList(i));

      srand (123145);
      EM em(singleton1->getEMMixtures(), digitData, i);

      long digitTime = em.learn(weights, means, variances);

      std::cout << "Learned digit " << i << " in " << digitTime << "ms" << std::endl;
    
    }
  }

  tbb::tick_count endTime = tbb::tick_count::now();
  
  long totalLearningTime = (long) ((endTime-startTime).seconds() * 1000);

  std::cout << "Total learning time: " << totalLearningTime << "ms"  << std::endl;

  return *pca;
}

int main(int argc, char* argv[]) {

  Reader r;
  r.readFile("data/train/image");
  
  LabelReader l;
  l.readFile("data/train/label");

  Reader r2;
  r2.readFile("data/test/image");
  
  LabelReader l2;
  l2.readFile("data/test/label");

  SingletonCust *singleton1;

  // initialisation des pointeurs
  singleton1 = SingletonCust::getInstance();

  if (argc > 2)
  {
    singleton1->setPCAComponents(atoi(argv[1]));
    singleton1->setEMMixtures(atoi(argv[2]));
  }

  if (argc > 3)
  {
    if (strlen(argv[3]) >= 3)
    {
      if (argv[3][0] == 'T')
        singleton1->setParNumber(true);

      if (argv[3][1] == 'T')
        singleton1->setParModel(true);

      if (argv[3][2] == 'T')
        singleton1->setParEtapeM(true);
    }
  }

  std::vector<Eigen::VectorXd> weights(10);
  std::vector<Eigen::MatrixXd> means(10);
  std::vector<std::vector<Eigen::MatrixXd>> variances(10);

  Eigen::MatrixXd data = r.getSampleMatrix();
  Pca& p = learnDataset(r, l, weights, means, variances);

  Eigen::MatrixXd reducedData = p.computePCATest(data.adjoint());
  reducedData.adjointInPlace();

  Classifier classifier(weights, means, variances);
  std::cout << "Classify Rate Train" << std::endl;
  classifier.classifyRate(reducedData, l);

  Eigen::MatrixXd test = r2.getSampleMatrix();
  Eigen::MatrixXd reducedTest = p.computePCATest(test.adjoint());
  reducedTest.adjointInPlace();

  std::cout << "Classify Rate Test" << std::endl;
  classifier.classifyRate(reducedTest, l2);

}
