#include "disp.hh"
void displayAsMatrix(Eigen::VectorXd& vector, int size) {

  for (int i = 0; i < vector.rows() / size; ++i) {
    for (int j = 0; j < vector.rows() && j < size; ++j) {
      std::cout << vector(i*size + j) << " ";
    }
    std::cout << std::endl;

  }

}

