#ifndef __EM__HH__
#define __EM__HH__

#include <Eigen/Dense>
#include <vector>
#include <cmath>
#include "logGaussian.hh"
#include "../singleton/singletoncust.hh"
#include <iostream>
#include <tbb/tbb.h>
#include <tbb/parallel_for.h>
#include <chrono>
#include "sum.hh"


class EM {
private:
  // Number of models in the gaussian mixture
  int modelNum;
  // Number of data samples
  int dataNum;
  // Number of components per sample
  int compNum; 
  
  // Weight of each Gaussian model
  //std::vector<double> weight;
  Eigen::VectorXd weight;

  // Probability of each Gaussian model being correct
  Eigen::MatrixXd probability;

  // Mean of each Gaussian model
  //std::vector<Eigen::VectorXd> mean;
  Eigen::MatrixXd mean;

  // Variance of each Gaussian model
  std::vector<Eigen::MatrixXd> variance;

  // Data
  //std::vector<Eigen::VectorXd> data;
  Eigen::MatrixXd data;

  double llk;

  int digit;

public:
  EM(int modelNum, Eigen::MatrixXd& data, int digit);

  void EtapeE();

  void EtapeM();

  double getLlk();

  Eigen::VectorXd& getWeight();
  Eigen::MatrixXd& getMean();
  std::vector<Eigen::MatrixXd>& getVariance();


long learn(std::vector<Eigen::VectorXd>& w,
	     std::vector<Eigen::MatrixXd>& m,
	     std::vector<std::vector<Eigen::MatrixXd>>& v);

};

#endif
