#ifndef __SUM__HH__
#define __SUM__HH__

#include <iostream>
#include <Eigen/Dense>
#include <tbb/tbb.h>

class Sum {
private:
  Eigen::MatrixXd value;
  int s;

public:

  Eigen::MatrixXd& data;
  Eigen::VectorXd& mean;
  Eigen::VectorXd& probability;

  Sum(int size, Eigen::MatrixXd& d,
      Eigen::VectorXd& m,
      Eigen::VectorXd& p) :
    data(d),
    mean(m),
    probability(p)
  {
    s = size;
    value = Eigen::MatrixXd::Zero(size, size);
    data = d;
    mean = m;
    probability = p;
  }

  Sum(Sum& r, tbb::split) : 
    data(r.data),
    mean(r.mean),
    probability(r.probability){
    s = r.s;
    value = Eigen::MatrixXd::Zero(r.s, r.s);
    data = r.data;
    mean = r.mean;
    probability = r.probability;
  }

  void operator() (const tbb::blocked_range<size_t>& r) {
    Eigen::MatrixXd temp = value;
    for (int j = r.begin(); j != r.end(); ++j) {
      temp += (data.col(j) - mean) * (data.col(j) - mean).adjoint() * probability(j);
    }
    value = temp;
  }

  void join(Sum& r) {
    value += r.value;
    data = r.data;
    mean = r.mean;
    probability = r.probability;
  }

  Eigen::MatrixXd getValue() {return value;}

};

#endif
