#include "logGaussian.hh"
#include <iostream>

double logGaussian(Eigen::VectorXd data, Eigen::VectorXd mean, Eigen::MatrixXd variance) {
  int d = data.rows();
  Eigen::VectorXd expVect = (data-mean).adjoint()*(variance.inverse())*(data-mean);

  double a = d*std::log(2*M_PI);
  double b = std::log(variance.determinant());
  double c = expVect(0);

  double res = (-0.5d) * (a + b + c);
  
 // std::cout << a << " " << a+b << " " << a+b+c << " " << res << std::endl;

    return res;
}


double gaussian(Eigen::VectorXd data, Eigen::VectorXd mean, Eigen::MatrixXd variance) {
  int d = data.rows();

  Eigen::VectorXd expVect = (data-mean).adjoint()*(variance.inverse())*(data-mean);

  double a = std::sqrt(std::pow(2*M_PI, d));
  double b = std::sqrt(variance.determinant());
  double c = std::exp(-0.5d * expVect(0));

  double res = c / (a * b);
  
 // std::cout << a << " " << a+b << " " << a+b+c << " " << res << std::endl;

    return res;
}