#ifndef __LOG_GAUSSIAN__HH__
#define __LOG_GAUSSIAN__HH__

#include <Eigen/Dense>
#include <Eigen/LU>
#include <cmath>

double logGaussian(Eigen::VectorXd data, Eigen::VectorXd mean, Eigen::MatrixXd variance);
double gaussian(Eigen::VectorXd data, Eigen::VectorXd mean, Eigen::MatrixXd variance);

#endif
