#include "EM.hh"

void sizeLog(Eigen::MatrixXd m, std::string name);

EM::EM(int modelNum, Eigen::MatrixXd& data, int digit) :
  modelNum(modelNum),
  weight(modelNum),
  data(data),
  digit(digit)
{
  this->data = data;

  sizeLog(data, "DATA");
  this->dataNum = data.cols();
  this->compNum = data.rows();
  for (int i = 0; i < this->modelNum; ++i)
  {
    weight(i) = (double) 1 / this->modelNum;
  }
  mean = Eigen::MatrixXd::Zero(this->compNum, this->modelNum);

  for (int i = 0; i < this->compNum; ++i)
  {
    for (int j = 0; j < this->modelNum; ++j)
    {
      mean(i, j) = (rand() % 100) / (double) 100;
    }
  }
 // mean = Eigen::MatrixXd::Random(this->compNum, this->modelNum);

//  std::cout << mean.topLeftCorner(10, 10) << std::endl;

  for (int i = 0; i < this->modelNum; ++i) {
    variance.push_back(Eigen::MatrixXd::Identity(this->compNum, this->compNum));
  }

  probability = Eigen::MatrixXd::Zero(this->dataNum, this->modelNum);

}

// ----

void EM::EtapeE() {

 SingletonCust *singleton3;

  // initialisation des pointeurs
  singleton3 = SingletonCust::getInstance();

  for (int j = 0; j < this->modelNum; ++j) { // for each model

    if (singleton3->getParModel())
    {

        parallel_for(tbb::blocked_range<size_t>(0, this->dataNum, 1), [&](const tbb::blocked_range<size_t> r){
          for(int i = r.begin(); i != r.end(); ++i) { // for each example
            probability(i, j) = std::log(weight(j))
            + logGaussian(data.col(i), mean.col(j), variance[j]);
           }
    
        });
    }
    else
    {
        for(int i = 0; i < this->dataNum; ++i) { // for each example
            probability(i, j) = std::log(weight(j))
           + logGaussian(data.col(i), mean.col(j), variance[j]);
          };

    }
  }


  Eigen::VectorXd TOTOVector = probability.rowwise().maxCoeff();
  Eigen::MatrixXd TOTO = TOTOVector.replicate(1, this->modelNum);

  probability -= TOTO;

  Eigen::MatrixXd expP = probability.array().exp();

  probability = expP.cwiseQuotient( (expP.rowwise().sum()).replicate(1, this->modelNum) );

  llk = ((expP.rowwise().sum()).array().log() + TOTOVector.array()).sum();

  //std::cout << "Log likelihood: " << llk << std::endl;

}

void EM::EtapeM() {

  tbb::concurrent_vector<Eigen::MatrixXd> var;

  Eigen::VectorXd W = probability.colwise().sum();

  unsigned int K = probability.cols();
  unsigned int N = probability.rows();
  unsigned int J = data.rows();

  Eigen::MatrixXd M(mean.rows(), mean.cols());

  for (unsigned int i = 0; i < K; ++i)
  {
    M.col(i) = (data * probability.col(i)) / W(i);
    Eigen::MatrixXd S = Eigen::MatrixXd::Zero(J, J);
    var.push_back(S);


    SingletonCust *singleton2;

    // initialisation des pointeurs
    singleton2 = SingletonCust::getInstance();


    if (singleton2->getParEtapeM())
    {
      Eigen::VectorXd m = mean.col(i);
      Eigen::VectorXd p = probability.col(i);
      Sum total(J, data, m, p);
      parallel_reduce(tbb::blocked_range<size_t>(0, N, 1), total);
      var[i] = total.getValue() ;
    }
    else
    {
      for (unsigned int j = 0; j < N; ++j)
      {
        var[i] = var[i] + (data.col(j) - mean.col(i)) * (data.col(j) - mean.col(i)).adjoint() * probability(j, i);
      }
    }
 
    var[i] = var[i] / W(i);
  }


  W = W / N;




double degenThreshold = 0.06;

for (unsigned int i = 0; i < K; ++i)
{

  if (W(i) <= degenThreshold)
  {
    std::cout << "degen: " << i << std::endl;
    int maxIndex = 0;
    double max = 0;

    for (int j = 0; j < W.rows(); ++j)
    {
      if (W(j) > max)
      {
        max = W(j);
        maxIndex = j;
      }
    }

    double newWeight = (W(i) + W(maxIndex)) / 2;
    W(maxIndex) = newWeight;
    W(i) = newWeight;
    
    M.col(i) = M.col(maxIndex) + Eigen::MatrixXd::Random(J,1) / 10000;
    var[i] = var[maxIndex] + Eigen::MatrixXd::Random(J,J) / 10000;
  }
}
  mean = M;
  variance = std::vector<Eigen::MatrixXd>(var.begin(), var.end());
  weight = W;
}

Eigen::VectorXd& EM::getWeight() {
  return weight;
}
Eigen::MatrixXd& EM::getMean() {
  return mean;
}
std::vector<Eigen::MatrixXd>& EM::getVariance() {
  return variance;
}

double EM::getLlk()
{
  return llk;
}

void sizeLog(Eigen::MatrixXd m, std::string name = "Matrix") {
  std::cout << name << ": " << m.rows() << " " << m.cols() << std::endl;
}

long EM::learn(std::vector<Eigen::VectorXd>& w,
	       std::vector<Eigen::MatrixXd>& m,
	       std::vector<std::vector<Eigen::MatrixXd>>& v) {
  tbb::tick_count startTime = tbb::tick_count::now();

  double llk = -std::numeric_limits<double>::infinity();
  double oldLlk = -std::numeric_limits<double>::infinity();

  while ((oldLlk == -std::numeric_limits<double>::infinity()) || (std::abs(oldLlk-llk) > 50))
    {
      oldLlk = llk;
      EtapeE();
      EtapeM();
      llk = getLlk();
    }

  w[digit] = getWeight();
  m[digit] = getMean();
  v[digit] = getVariance();

  tbb::tick_count endTime = tbb::tick_count::now();
  
  return (long) ((endTime-startTime).seconds() * 1000); // result given in milliseconds
}
