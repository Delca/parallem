#ifndef __CLASSIFY__HH__
#define __CLASSIFY__HH__

#include <vector>
#include <iostream>
#include <Eigen/Dense>
#include "../em/logGaussian.hh"
#include <algorithm>
#include "../reader/labelReader.hh"

class Classifier {
private:

  std::vector<Eigen::VectorXd>& weights;
  std::vector<Eigen::MatrixXd>& means;
  std::vector<std::vector<Eigen::MatrixXd>>& variances;

public:
  Classifier(std::vector<Eigen::VectorXd>& weights,
	     std::vector<Eigen::MatrixXd>& means,
	     std::vector<std::vector<Eigen::MatrixXd>>& variances);

  int classify(Eigen::VectorXd sample);
  void classifyRate(Eigen::MatrixXd& sampleMatrix, LabelReader& labelReader);

};

#endif
