#include "classify.hh"

Classifier::Classifier(std::vector<Eigen::VectorXd>& weights,
	   std::vector<Eigen::MatrixXd>& means,
	   std::vector<std::vector<Eigen::MatrixXd>>& variances) :
  weights(weights),
  means(means),
  variances(variances)
{
}

int Classifier::classify(Eigen::VectorXd sample) {
  double dist[10];

  // This assumes that the number of
  // models in each mixture is the same
  
  // Number of components in a sample
  int l = means[0].rows();
  // Number of models in a mixture
  int k = means[0].cols();
  // Number of classes
  int c = means.size();


  for (int i = 0; i < c; ++i) {
    double t = 0;
    Eigen::VectorXd& w = weights[i];

    for (int j = 0; j < k; ++j) {
      Eigen::VectorXd m = means[i].col(j);
      Eigen::MatrixXd s = variances[i][j];
      t += w(j) * gaussian(sample, m, s);
    }

    dist[i] = t;
  //  std::cout << i << " " << t << std::endl;
  }

  int classIndex = std::distance(dist, std::max_element(dist, dist+10));

  int maxIndex = 0;
  double max = 0;

   for (int j = 0; j < 10; ++j)
    {
      if (dist[j] > max)
      {
        max = dist[j];
        maxIndex = j;
      }
    }

  return maxIndex;
}

void Classifier::classifyRate(Eigen::MatrixXd& sampleMatrix, LabelReader& labelReader) {
  int recognized = 0;

  for (int i = 0; i < sampleMatrix.cols(); ++i) {
    //std::cout << classify((Eigen::VectorXd) sampleMatrix.col(i)) << " " << labelReader.getDigit(i) << std::endl;
    recognized += ( classify((Eigen::VectorXd) sampleMatrix.col(i)) == labelReader.getDigit(i)  );
  }
  std::cout << "Recognition rate: " << (double) recognized / sampleMatrix.cols() << std::endl;

}
