#ifndef PCA__HH__
#define PCA__HH__

#include <Eigen/Core>
#include <Eigen/Eigen>
#include <iostream>
#include <tbb/tbb.h>
#include <tbb/parallel_for.h>

class Pca 
{
public:
  Pca();
  virtual ~Pca();

  Eigen::MatrixXd computePCA(Eigen::MatrixXd DataPoints, unsigned int dimPCA);
  Eigen::MatrixXd computePCATest(Eigen::MatrixXd DataTest);
  Eigen::VectorXd unPCA(Eigen::VectorXd data);
  Eigen::MatrixXd getVS();

private:
	Eigen::MatrixXd VS;
 

};

#endif
