#include "pca.hh"

Pca::Pca()
{

}

Pca::~Pca()
{

}

Eigen::MatrixXd Pca::computePCA(Eigen::MatrixXd DataPoints, unsigned int dimPCA)
{
	unsigned int dimSpace = dimPCA; // dimension space
	unsigned int m = DataPoints.rows();   // number of images
	unsigned int n = DataPoints.cols();  // dimension of each images
	
	double mean; 
	Eigen::VectorXd meanVector;
	Eigen::MatrixXd centered = Eigen::MatrixXd::Zero(m, n);;

	typedef std::pair<double, int> myPair;
	typedef std::vector<myPair> PermutationIndices;	


	// Covariance v1
	//
	// for each point
	//   center the poin with the mean among all the coordinates
	//

	// for (int i = 0; i < DataPoints.cols(); i++){
	// 	mean = (DataPoints.col(i).sum())/m;		 //compute mean
	// 	meanVector  = Eigen::VectorXd::Constant(m,mean); // create a vector with constant value = mean
	// 	centered.col(i) = DataPoints.col(i) - meanVector;
	// 	//	std::cout << meanVector.transpose() << "\n" << DataPoints.col(i).transpose() << "\n\n";
	// }
	//std::cout << "done1" << std::endl;
	// get the covariance matrix
	//Eigen::MatrixXd Covariance = Eigen::MatrixXd::Zero(m, n);
	//Covariance = (1 / (double) m) *  DataPoints.adjoint() * DataPoints2;
	//  Std::cout << Covariance ;	
	
	
	centered = DataPoints.rowwise() - DataPoints.colwise().mean();
	Eigen::MatrixXd Covariance = (centered.adjoint() * centered) / double(DataPoints.rows());
	
	//std::cout << Covariance << std::endl;
	std::cout << "Covariance done" << std::endl;

	// compute the eigenvalue on the Cov Matrix
	Eigen::EigenSolver<Eigen::MatrixXd> m_solve(Covariance);


	Eigen::VectorXd eigenvalues = Eigen::VectorXd::Zero(n);
	eigenvalues = m_solve.eigenvalues().real();

	Eigen::MatrixXd eigenVectors = Eigen::MatrixXd::Zero(m, n);  // matrix (m x n) (images, dims)
	eigenVectors = m_solve.eigenvectors().real();	

	// sort and get the permutation indices
	PermutationIndices pi;

	for (int i = 0 ; i < n; i++)
	  pi.push_back(std::make_pair(eigenvalues(i), i));

	sort(pi.begin(), pi.end());

	/*
	for (unsigned int i = 0; i < n ; i++)
	  std::cout << "eigen=" << pi[i].first << " pi=" << pi[i].second << std::endl;
	*/


	// consider the subspace corresponding to the top-k eigenvectors
	unsigned int i = pi.size() - 1;
	unsigned int howMany = i - dimSpace;


	this->VS = Eigen::MatrixXd::Zero(eigenVectors.col(0).size(), dimSpace);


	for (unsigned int j = 0; i > howMany; i--, j++)
	{
	  //std::cout << pi[i].first << "-eigenvector " << eigenVectors.col(pi[i].second) << "\n";
	  VS.col(j) = eigenVectors.col(pi[i].second);
	}
/*
	for (int i = 0; i < DataPoints.cols(); i++)
	{
	   mean = (DataPoints.col(i).sum()) / m;		 //compute mean
	   meanVector = Eigen::VectorXd::Constant(m, mean); // create a vector with constant value = mean
	   DataPoints.col(i) -= meanVector;
	 
	}
*/
	Eigen::MatrixXd DataPointsPCA = centered * VS;

	//std::cout << DataPointsPCA.row(1) << std::endl;

	return DataPointsPCA;
}


Eigen::MatrixXd Pca::computePCATest(Eigen::MatrixXd DataTest)
{
	Eigen::MatrixXd centered = DataTest.rowwise() - DataTest.colwise().mean();
	Eigen::MatrixXd DataPointsPCA = centered * VS;


	return DataPointsPCA;
}

Eigen::MatrixXd Pca::getVS()
{
	return VS;
}


int main2()
{
	Eigen::MatrixXd DataPoints = Eigen::MatrixXd::Random(4, 3);  // matrix (m x n)

	DataPoints(0,0) = 1;
	DataPoints(0,1) = 6;
	DataPoints(0,2) = 15;
	DataPoints(1,0) = 4;
	DataPoints(1,1) = 18;
	DataPoints(1,2) = 6;
	DataPoints(2,0) = 2;
	DataPoints(2,1) = 13;
	DataPoints(2,2) = 5;
	DataPoints(3,0) = 11;
	DataPoints(3,1) = 12;
	DataPoints(3,2) = 1;
	


	Pca p = Pca();
	p.computePCA(DataPoints, 2);
}

Eigen::VectorXd Pca::unPCA(Eigen::VectorXd data) {
  return VS * data;
}
