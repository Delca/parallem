#ifndef __SINGLETONCUST__HH__
#define __SINGLETONCUST__HH__


#include <iostream>

class SingletonCust
{
  private:
    // Constructeur/destructeur
    SingletonCust ()
      : PCAComponents (20),
        EMMixtures (2),
        ParNumber (false),
        ParModel (false),
        ParEtapeM (false)
       {
       }
    ~SingletonCust () { }

  public:
    // Interface publique
    void setPCAComponents (int val) { PCAComponents = val; }
    int getPCAComponents () { return PCAComponents; }

    void setEMMixtures (int val) { EMMixtures = val; }
    int getEMMixtures () { return EMMixtures; }

    void setParNumber (bool val) { ParNumber = val; }
    bool getParNumber () { return ParNumber; }

    void setParModel (bool val) { ParModel = val; }
    bool getParModel () { return ParModel; }

    void setParEtapeM (bool val) { ParEtapeM = val; }
    bool getParEtapeM () { return ParEtapeM; }



    // Fonctions de création et destruction du singleton
    static SingletonCust *getInstance ()
    {
      if (nullptr == _singleton)
        {
       //   std::cout << "creating singleton." << std::endl;
          _singleton =  new SingletonCust;
        }
      else
        {
    //     std::cout << "singleton already created!" << std::endl;
        }

      return _singleton;
    }

    static void kill ()
    {
      if (nullptr != _singleton)
        {
          delete _singleton;
          _singleton = nullptr;
        }
    }

  private:
    // Variables membres
    int PCAComponents;
    int EMMixtures;
    bool ParNumber;
    bool ParModel;
    bool ParEtapeM;
    
    static SingletonCust *_singleton;
};

#endif