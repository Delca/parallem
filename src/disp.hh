#ifndef __DISP__HH__
#define __DISP__HH__
#include <iostream>
#include <Eigen/Dense>

void displayAsMatrix(Eigen::VectorXd& vector, int size);

#endif
