#ifndef __READ_INT_32_CC__
#define __READ_INT_32_CC__

#include <cstdint>
#include <fstream>

int32_t readInt32(std::ifstream& stream);

#endif
