#ifndef __LABEL_READER__HH__
#define __LABEL_READER__HH__

#include <fstream>
#include <iostream>
#include <cstdint>
#include <sys/stat.h>
#include "readInt32.hh"
#include <vector>
#include <map>

class LabelReader {
private:
  
  std::ifstream file;
  int32_t dataNum;
  std::map<int, std::vector<int>> digitList;
  char* digits;

public:
  LabelReader();

  void readFile(std::string filename);
  std::vector<int> getIndexList(int digit);
  int getDigit(int position);

};

#endif
