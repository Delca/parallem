#include "readInt32.hh"

int32_t readInt32(std::ifstream& stream) {
  int32_t ans = 0;

  stream.read((char*)&ans + 3, sizeof(char));
  stream.read((char*)&ans + 2, sizeof(char));
  stream.read((char*)&ans + 1, sizeof(char));
  stream.read((char*)&ans + 0, sizeof(char));

  return ans;
}
