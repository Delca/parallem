#ifndef __READER__HH__
#define __READER__HH__

#include <Eigen/Dense>
#include <fstream>
#include <iostream>
#include <cstdint>
#include <sys/stat.h>
#include "readInt32.hh"
#include <vector>

class Reader {
private:

  std::ifstream file;
  int32_t rowNum;
  int32_t colNum;
  int32_t dataNum;

public:

  Reader();

  void readFile(std::string filename);
  Eigen::MatrixXd& getNextImageAsMatrix();
  Eigen::MatrixXd& getImageAsMatrix(int i);

  Eigen::VectorXd& getNextImageAsVector();
  Eigen::VectorXd& getImageAsVector(int i);

  Eigen::MatrixXd& getSampleMatrix();
  Eigen::MatrixXd& getDigitMatrix(std::vector<int> samples);
  

};

#endif
