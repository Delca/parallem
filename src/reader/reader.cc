#include "reader.hh"

Reader::Reader() {
  this->rowNum = 0;
  this->colNum = 0;
}

void Reader::readFile(std::string filename) {
  struct stat buffer;
  // Fast way to check for file accessibility
  // -> see http://stackoverflow.com/questions/12774207
  if (stat(filename.c_str(), &buffer) == 0){
    this->file.open(filename, std::ios::in | std::ios::binary);

    std::cout << readInt32(this->file) << std::endl;
    dataNum = readInt32(this->file);
    rowNum = readInt32(this->file);
    colNum = readInt32(this->file);

  }
  else {
    std::cerr << "An error has occurred while trying to open the file " << filename << std::endl;
  }


}

Eigen::MatrixXd& Reader::getNextImageAsMatrix() {
  Eigen::MatrixXd* ans = new Eigen::MatrixXd(this->rowNum, this->colNum);
  char* image = (char*) malloc(rowNum * colNum * sizeof(char));
  this->file.read(image, rowNum * colNum * sizeof(char));
  
  for (int i = 0; i < rowNum; ++i) {
      for (int j = 0; j < colNum; ++j) {
	(*ans)(i, j) = *(image + j + i*colNum);
	std::cout << (int) (unsigned char) *(image + j + i*colNum) << " ";
      }
      std::cout << std::endl;
  }

  free(image);

  return *ans;
}

Eigen::MatrixXd& Reader::getImageAsMatrix(int i) {
  long position = (4 + 4 + 4 + 4 + i*rowNum*colNum) * sizeof(char);

  this->file.seekg(position);

  return getNextImageAsMatrix();
}

// ----


Eigen::VectorXd& Reader::getNextImageAsVector() {
  Eigen::VectorXd* ans = new Eigen::VectorXd(this->rowNum * this->colNum);
  char* image = (char*) malloc(rowNum * colNum * sizeof(char));
  this->file.read(image, rowNum * colNum * sizeof(char));
  
  for (int i = 0; i < rowNum * colNum; ++i) {
	(*ans)(i) = *(image + i);
  }

  free(image);

  return *ans;
}

Eigen::VectorXd& Reader::getImageAsVector(int i) {
  long position = (4 + 4 + 4 + 4 + i*rowNum*colNum) * sizeof(char);

  this->file.seekg(position);

  return getNextImageAsVector();
}

Eigen::MatrixXd& Reader::getSampleMatrix() {
  Eigen::MatrixXd* ans = new Eigen::MatrixXd(this->rowNum * this->colNum, this->dataNum);
  int compNum = rowNum*colNum;

  char* image = (char*) malloc(compNum * dataNum * sizeof(char));
  this->file.seekg(4*4*sizeof(char));
  this->file.read(image, compNum * dataNum * sizeof(char));
  

  for (int i = 0; i < rowNum * colNum * dataNum; ++i) {
    (*ans)(i % compNum, i/compNum) = (unsigned char) *(image + i);
  }

  free(image);

  return *ans;
}

Eigen::MatrixXd& Reader::getDigitMatrix(std::vector<int> samples) {
  Eigen::MatrixXd* ans = new Eigen::MatrixXd(this->rowNum * this->colNum, samples.size());
  int compNum = rowNum*colNum;
  char* image = (char*) malloc(compNum * sizeof(char));

  for (int i = 0; i < samples.size(); ++i) {
    this->file.seekg(4*4 + samples[i]*compNum);
    this->file.read(image, compNum * sizeof(char));

    for (int j = 0; j < compNum; ++j) {
      (*ans)(j, i) = *(image + j);
    }

  }

  free(image);

  return *ans;
}
