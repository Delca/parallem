#include "labelReader.hh"

LabelReader::LabelReader() {
  for (int i = 0; i < 10; ++i) {
    digitList[i] = std::vector<int>();
  }
}


void LabelReader::readFile(std::string filename) {
  struct stat buffer;
  // Fast way to check for file accessibility
  // -> see http://stackoverflow.com/questions/12774207
  if (stat(filename.c_str(), &buffer) == 0) {
    this->file.open(filename, std::ios::in | std::ios::binary);

    std::cout << readInt32(this->file) << std::endl;
    dataNum = readInt32(this->file);
    digits = (char*) malloc(dataNum * sizeof(char));
    for (int i = 0; i < dataNum; ++i) {
      int digit = 0;

      this->file.read((char*) &digit, sizeof(char));

      digitList[digit].push_back(i);
      
    }

    this->file.seekg(4*2*sizeof(char));
    this->file.read(digits, dataNum*sizeof(char));

  }
  else {
    std::cerr << "An error has occurred while trying to open the file " << filename << std::endl;
  }
}

std::vector<int> LabelReader::getIndexList(int digit) {
  return this->digitList[digit];
}

int LabelReader::getDigit(int position) {
  return digits[position];
}
