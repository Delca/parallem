all:
	clear
	g++ src/*.cc src/*/*.cc -Ilib -o parallem -ltbb -std=c++0x -O3 -DNDEBUG -g -pg -ggdb3
clean:
	rm -f *~
	rm -f parallem
