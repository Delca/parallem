function [res] = RealGaussian(x, m, s)
d = length(x);
res = 1/( (2*pi)^(d/2) * sqrt(det(s)) ) * exp( -(1/2) * (x'-m)'*inv(s)*(x'-m) );
end
