clc;
clear;
close all
load data;

C = 10; % nombre de classe
K = 2; % nombre de composante du melange par classe
vectorSize = 20; % taille d' un exemple apres PCA

t = double(reshape(TRAIN, 60000, 28*28));
test = double(reshape(TEST, 10000, 28*28));
Co = cov(t, 1);
[V, D] = eig(Co);
d = diag(D);
[dsort, I] = sort(d, 1, 'descend'); 
vsort = V(:, I);
VS = vsort(:, 1:vectorSize);

TRAINB = (t - repmat(mean(t), 60000, 1))*VS;
TESTB = (test - repmat(mean(test), 10000, 1))*VS;


 m = zeros(vectorSize, K, C);
 w = zeros(K, C);
 s = zeros(vectorSize, vectorSize, K, C);
 
 seeds = eye(1, 10) * 1053;
 seeds(1 + 1) = 42;
 seeds(4 + 1) = 42;
 seeds(6 + 1) = 42;
 seeds(9 + 1) = 42;
 
 
 startI = 0;
 endI = 9;
 ind1 = find(LABEL_TRAIN == i);
 for i=startI:endI
     rng(seeds(i+1));
     figure;
     hold on;
     
     M = rand(vectorSize, K);
     S = zeros(vectorSize, vectorSize, K);
     for j=1:K
         S(:, :, j) = eye(vectorSize, vectorSize);     
     end
     
     ind = find(LABEL_TRAIN == i);
     size(ind)
     [model, w(:,i+1), covar] = EM(TRAINB(ind, :), M, K, S);
     
     disp(sprintf('Trained components for %d', i));
     
     for k=1:K
        m(:, k, i+1) = model(:, k); 
        s(:, :, k, i+1) = covar(:, :, k);
     end
     
     L=reshape(VS*m(:, :, i+1), 28, 28, K);
     for i=1:K
         subplot(2, 4, i);
         imshow(L(:, :, i));
     end;
     
end

save('trained_parameters', 'm', 'w', 's');
disp('Trained parameters saved!');

% train
meanClassified = logical(zeros(60000, 1));
for i=1:60000
    meanClassified(i) = (classifyByMeans(TRAINB(i, :), m, w, s) - 1) == LABEL_TRAIN(i);
end
sum(meanClassified)/60000
%test
meanClassified = logical(zeros(10000, 1));
for i=1:10000
    meanClassified(i) = (classifyByMeans(TESTB(i, :), m, w, s) - 1) == LABEL_TEST(i);
end
sum(meanClassified)/10000
