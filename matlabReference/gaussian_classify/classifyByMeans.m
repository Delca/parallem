function [class] = classifyByMeans(x, m, w, s)
% m(:, i) contains the mean of all data for class i
% l: composantes de la moyenne de bernoulli
% k: moyenne (modele) de bernoulli de la classe c
% c: classe (chiffre a reconnaitre)
[l, k, c] = size(m);
dist = zeros(1, c);

for i=1:c
    t = 0;
    for j=1:k
        t = t + w(j,i) * RealGaussian(x, m(:, j, i), s(:, :, j, i));
    end
    dist(i) = t;
end

[val, ind] = max(dist);
class = ind;
end
