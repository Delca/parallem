function [M W S] = tp3(T, M, K, S)

modelNum = K;
[l vectorSize] = size(T);

% poids des modeles de Bernoulli
W = repmat(1/modelNum, [1 modelNum]);
% centres des modeles de Bernoulli, initialise au hasard
% probabilites des modeles de Bernoulli
P = zeros(vectorSize, modelNum);


for i=1:10
   P = EtapeE(T, W, M, S);
   [W, M, S] = EtapeM(T, P);
end

end


