function res = Gaussian(x, m, s)
d = length(x);
res = -(1/2)* ( d*log(2*pi) + log(det(s)) + (x'-m)'*inv(s)*(x'-m)  );

end