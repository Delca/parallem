function P = EtapeE(X, W, M, S)
[N, D] = size(X);
K = length(W);
[D1, K1] = size(M);
assert(D1==D);
assert(K1==K);
P=zeros(N,K);

for i=1:N % pour tous les exemples
   for j=1:K % pour tous les modeles 
       % probleme de de precision 
       % P(i, j) = W(j) * Bernoulli( X(i, :), M(:, j) );
       P(i, j) = log(W(j)) + Gaussian( X(i, :), M(:, j), S(:, :, j) );
   
   end
end
disp 'GAUSSIAN COMPUTED'

% probleme de precision fix
TOTO = max(P, [], 2);
P = P - repmat(TOTO, 1, K);
% END probleme de precision fix


% calcul du log de vraisemblance
% cette valeur doit �tre n�gative et cro�tre strictement
%llk = sum(P, 2);
%P = P ./ repmat(llk, 1, K); % on normalise la matrice des poids
%llk = sum(log(llk));
%disp(sprintf('llk = %f', llk));

% probleme de precision fix

s2 = log(sum(exp(P),2)) + TOTO;
P= exp(P) ./ (repmat(sum(exp(P),2),1, K));
disp(['Log Likelihood = ', num2str(sum(s2))]);

% disp(sprintf('TOTO = %f', TOTO));
% END probleme de precision fix

end