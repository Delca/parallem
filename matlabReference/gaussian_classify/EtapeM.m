function [W M S] = EtapeM(X, P)
[N, K] = size(P);
[I, J] = size(X);
S = zeros(J, J, K);
W = sum(P, 1);
for i=1:K
    %   M(:, i) = sum(  repmat(P(:, K)' , D, 1).*X;
    % equivaut a la multiplication matrice-vecteur suivante
    M(:, i) = ( X' * P(:, i) ) / W(i);
    
    for j=1:N
        S(:, :, i) = S(:, :, i) + (X(j, :)'-M(:, i))*(X(j, :)'-M(:, i))' * P(j, i);
    end
    
    S(:, :, i) = S(:, :, i)/W(i);
   
end

W = W / N;

degenThreshold = 0.05;

for i=1:K
if W(i) <= degenThreshold
   
    [val ind] = max(W);
    newWeight = (W(i) + W(ind))/2;
    W(ind) = newWeight;
    W(i) = newWeight;
    
    M(:, i) = M(:, ind) + rand(J, 1)/10000;
    S(:, :, i) = S(:, :, ind) + rand(J, J)/10000;
    
end

end